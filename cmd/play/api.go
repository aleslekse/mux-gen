package main

import (
	"encoding/json"
	"errors"
	muxgen "gitlab.com/aleslekse/mux-gen/mux_gen"
	"net/http"
	"net/url"
	"time"
)

type Status string

const (
	StatusCreated  = "created"
	StatusUpdated  = "updated"
	StatusNotified = "notified"
)

var statusToString = map[Status]string{
	StatusCreated:  "created",
	StatusNotified: "notified",
	StatusUpdated:  "updated",
}
var stringToStatus = map[string]Status{
	"created":  StatusCreated,
	"notified": StatusNotified,
	"updated":  StatusUpdated,
}

func (e Status) Valid() bool {
	_, ok := stringToStatus[string(e)]
	return ok
}
func (e Status) String() string {
	s, _ := statusToString[e]
	return s
}

type Address struct {
	Street  string  `json:"street"`
	ZipCode *string `json:"zipCode"`
	City    *string `json:"city"`
	Tag     *Tag    `json:"tag"`
}

func (m *Address) QueryPresent(prefix string, v url.Values) bool {
	if v.Has(prefix + "street") {
		return true
	}
	if v.Has(prefix + "zipCode") {
		return true
	}
	if v.Has(prefix + "city") {
		return true
	}
	if (*Tag).QueryPresent(nil, prefix+"tag.", v) {
		return true
	}
	return false
}
func (m *Address) QueryUnmarshall(prefix string, v url.Values) (err error) {
	m.Street, err = muxgen.UnmarshallStringValue(v.Get(prefix+"street"), true)
	if err != nil {
		return
	}
	m.ZipCode, err = muxgen.UnmarshallStringPointer(v.Get(prefix+"zipCode"), false)
	if err != nil {
		return
	}
	m.City, err = muxgen.UnmarshallStringPointer(v.Get(prefix+"city"), false)
	if err != nil {
		return
	}
	if m.Tag.QueryPresent(prefix+"tag.", v) {
		m.Tag = new(Tag)
		err = m.Tag.QueryUnmarshall(prefix+"tag.", v)
		if err != nil {
			return
		}
	}
	return
}

type Author struct {
	FullName string `json:"fullName"`
	Address
}

func (m *Author) QueryPresent(prefix string, v url.Values) bool {
	if v.Has(prefix + "fullName") {
		return true
	}
	if (*Address).QueryPresent(nil, prefix, v) {
		return true
	}
	return false
}
func (m *Author) QueryUnmarshall(prefix string, v url.Values) (err error) {
	m.FullName, err = muxgen.UnmarshallStringValue(v.Get(prefix+"fullName"), false)
	if err != nil {
		return
	}
	err = m.Address.QueryUnmarshall(prefix, v)
	if err != nil {
		return
	}
	return
}

type CreatePageBody struct {
	Page
	Id int `json:"id"`
}

func (m *CreatePageBody) QueryPresent(prefix string, v url.Values) bool {
	if (*Page).QueryPresent(nil, prefix, v) {
		return true
	}
	if v.Has(prefix + "id") {
		return true
	}
	return false
}
func (m *CreatePageBody) QueryUnmarshall(prefix string, v url.Values) (err error) {
	err = m.Page.QueryUnmarshall(prefix, v)
	if err != nil {
		return
	}
	m.Id, err = muxgen.UnmarshallIntegerValue(v.Get(prefix+"id"), false)
	if err != nil {
		return
	}
	return
}

type CreatePageSearch struct {
	SessionId string `json:"sessionId"`
}

func (m *CreatePageSearch) QueryPresent(prefix string, v url.Values) bool {
	if v.Has(prefix + "sessionId") {
		return true
	}
	return false
}
func (m *CreatePageSearch) QueryUnmarshall(prefix string, v url.Values) (err error) {
	m.SessionId, err = muxgen.UnmarshallStringValue(v.Get(prefix+"sessionId"), false)
	if err != nil {
		return
	}
	return
}

type GetPagesSearch struct {
	UserId int    `json:"userId"`
	Sort   string `json:"sort"`
	Dir    string `json:"dir"`
	Limit  int    `json:"limit"`
	*Address
	Status  *Status  `json:"status"`
	Licence *Licence `json:"licence"`
}

func (m *GetPagesSearch) QueryPresent(prefix string, v url.Values) bool {
	if v.Has(prefix + "userId") {
		return true
	}
	if v.Has(prefix + "sort") {
		return true
	}
	if v.Has(prefix + "dir") {
		return true
	}
	if v.Has(prefix + "limit") {
		return true
	}
	if (*Address).QueryPresent(nil, prefix, v) {
		return true
	}
	if v.Has(prefix + "status") {
		return true
	}
	if (*Licence).QueryPresent(nil, prefix+"licence.", v) {
		return true
	}
	return false
}
func (m *GetPagesSearch) QueryUnmarshall(prefix string, v url.Values) (err error) {
	m.UserId, err = muxgen.UnmarshallIntegerValue(v.Get(prefix+"userId"), false)
	if err != nil {
		return
	}
	m.Sort, err = muxgen.UnmarshallStringValue(v.Get(prefix+"sort"), false)
	if err != nil {
		return
	}
	m.Dir, err = muxgen.UnmarshallStringValue(v.Get(prefix+"dir"), false)
	if err != nil {
		return
	}
	m.Limit, err = muxgen.UnmarshallIntegerValue(v.Get(prefix+"limit"), false)
	if err != nil {
		return
	}
	if m.Address.QueryPresent(prefix, v) {
		m.Address = new(Address)
		err = m.Address.QueryUnmarshall(prefix, v)
		if err != nil {
			return
		}
	}
	statusStr, err := muxgen.UnmarshallStringPointer(v.Get(prefix+"status"), false)
	if err != nil {
		return err
	}
	if statusStr != nil {
		m.Status = (*Status)(statusStr)
		if !m.Status.Valid() {
			return errors.New("invalid status value")
		}
	}
	if m.Licence.QueryPresent(prefix+"licence.", v) {
		m.Licence = new(Licence)
		err = m.Licence.QueryUnmarshall(prefix+"licence.", v)
		if err != nil {
			return
		}
	}
	return
}

type Licence struct {
	Did   string `json:"did"`
	Valid bool   `json:"valid"`
}

func (m *Licence) QueryPresent(prefix string, v url.Values) bool {
	if v.Has(prefix + "did") {
		return true
	}
	if v.Has(prefix + "valid") {
		return true
	}
	return false
}
func (m *Licence) QueryUnmarshall(prefix string, v url.Values) (err error) {
	m.Did, err = muxgen.UnmarshallStringValue(v.Get(prefix+"did"), false)
	if err != nil {
		return
	}
	m.Valid, err = muxgen.UnmarshallBooleanValue(v.Get(prefix+"valid"), false)
	if err != nil {
		return
	}
	return
}

type Page struct {
	Id    int      `json:"id"`
	Title string   `json:"title"`
	Alias []string `json:"alias"`
}

func (m *Page) QueryPresent(prefix string, v url.Values) bool {
	if v.Has(prefix + "id") {
		return true
	}
	if v.Has(prefix + "title") {
		return true
	}
	return false
}
func (m *Page) QueryUnmarshall(prefix string, v url.Values) (err error) {
	m.Id, err = muxgen.UnmarshallIntegerValue(v.Get(prefix+"id"), false)
	if err != nil {
		return
	}
	m.Title, err = muxgen.UnmarshallStringValue(v.Get(prefix+"title"), false)
	if err != nil {
		return
	}
	return
}

type Tag struct {
	Id        int       `json:"id"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"createdAt"`
}

func (m *Tag) QueryPresent(prefix string, v url.Values) bool {
	if v.Has(prefix + "id") {
		return true
	}
	if v.Has(prefix + "name") {
		return true
	}
	if v.Has(prefix + "createdAt") {
		return true
	}
	return false
}
func (m *Tag) QueryUnmarshall(prefix string, v url.Values) (err error) {
	m.Id, err = muxgen.UnmarshallIntegerValue(v.Get(prefix+"id"), false)
	if err != nil {
		return
	}
	m.Name, err = muxgen.UnmarshallStringValue(v.Get(prefix+"name"), false)
	if err != nil {
		return
	}
	if v.Has(prefix + "createdAt") {
		err = m.CreatedAt.UnmarshalText([]byte(v.Get(prefix + "createdAt")))
		if err != nil {
			return
		}
	}
	return
}

type Api interface {
	// CreatePage implements POST handler for /pages/:user
	CreatePage(request *http.Request, user int, search CreatePageSearch, body CreatePageBody) (*Page, error)
	// GetPageById implements GET handler for /pages/:id
	GetPageById(request *http.Request, id int, search Tag) (*Page, error)
	// GetPages implements GET handler for /pages/all/:userId
	GetPages(request *http.Request, userId int, search GetPagesSearch) (*Page, error)
	// MyCustom implements GET handler for /custom
	MyCustom(request *http.Request, writer http.ResponseWriter)
	// PageHelp implements GET handler for /pages
	PageHelp(request *http.Request, writer http.ResponseWriter)
	Error(r *http.Request, w http.ResponseWriter, err error)
}

func NewApiHandler(routes Api) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		var idx int
		var name string
		var vars []string
		switch request.Method {
		case "GET":
			idx, name = muxgen.SplitPath(idx, request.URL.Path)
			if idx == -1 {
				routes.Error(request, writer, muxgen.ErrNotFound)
				return
			}
			switch name {
			case "custom":
				idx, name = muxgen.SplitPath(idx, request.URL.Path)
				if idx == -1 {
					routes.MyCustom(request, writer)
					return
				}
			case "pages":
				idx, name = muxgen.SplitPath(idx, request.URL.Path)
				if idx == -1 {
					routes.PageHelp(request, writer)
					return
				}
				switch name {
				case "all":
					idx, name = muxgen.SplitPath(idx, request.URL.Path)
					if idx == -1 {
						routes.Error(request, writer, muxgen.ErrNotFound)
						return
					}
					switch name {
					default:
						vars = append(vars, name)
						idx, name = muxgen.SplitPath(idx, request.URL.Path)
						if idx == -1 {
							varUserId0, err := muxgen.UnmarshallIntegerValue(vars[0], false)
							if err != nil {
								routes.Error(request, writer, err)
								return
							}
							var varSearch1 GetPagesSearch
							err = varSearch1.QueryUnmarshall("", request.URL.Query())
							if err != nil {
								routes.Error(request, writer, err)
								return
							}
							var varResponse3 *Page
							varResponse3, err = routes.GetPages(request, varUserId0, varSearch1)
							if err != nil {
								routes.Error(request, writer, err)
								return
							}
							writer.Header().Set("Content-Type", "application/json")
							err = json.NewEncoder(writer).Encode(varResponse3)
							if err != nil {
								routes.Error(request, nil, err)
								return
							}
							return
						}
					}
				default:
					vars = append(vars, name)
					idx, name = muxgen.SplitPath(idx, request.URL.Path)
					if idx == -1 {
						varId0, err := muxgen.UnmarshallIntegerValue(vars[0], false)
						if err != nil {
							routes.Error(request, writer, err)
							return
						}
						var varSearch1 Tag
						err = varSearch1.QueryUnmarshall("", request.URL.Query())
						if err != nil {
							routes.Error(request, writer, err)
							return
						}
						var varResponse3 *Page
						varResponse3, err = routes.GetPageById(request, varId0, varSearch1)
						if err != nil {
							routes.Error(request, writer, err)
							return
						}
						writer.Header().Set("Content-Type", "application/json")
						err = json.NewEncoder(writer).Encode(varResponse3)
						if err != nil {
							routes.Error(request, nil, err)
							return
						}
						return
					}
				}
			}
		case "POST":
			idx, name = muxgen.SplitPath(idx, request.URL.Path)
			if idx == -1 {
				routes.Error(request, writer, muxgen.ErrNotFound)
				return
			}
			switch name {
			case "pages":
				idx, name = muxgen.SplitPath(idx, request.URL.Path)
				if idx == -1 {
					routes.Error(request, writer, muxgen.ErrNotFound)
					return
				}
				switch name {
				default:
					vars = append(vars, name)
					idx, name = muxgen.SplitPath(idx, request.URL.Path)
					if idx == -1 {
						varUser0, err := muxgen.UnmarshallIntegerValue(vars[0], false)
						if err != nil {
							routes.Error(request, writer, err)
							return
						}
						var varSearch1 CreatePageSearch
						err = varSearch1.QueryUnmarshall("", request.URL.Query())
						if err != nil {
							routes.Error(request, writer, err)
							return
						}
						var varBody2 CreatePageBody
						err = json.NewDecoder(request.Body).Decode(&varBody2)
						if err != nil {
							routes.Error(request, writer, err)
							return
						}
						var varResponse3 *Page
						varResponse3, err = routes.CreatePage(request, varUser0, varSearch1, varBody2)
						if err != nil {
							routes.Error(request, writer, err)
							return
						}
						writer.Header().Set("Content-Type", "application/json")
						err = json.NewEncoder(writer).Encode(varResponse3)
						if err != nil {
							routes.Error(request, nil, err)
							return
						}
						return
					}
				}
			}
		}
		return
	}
}
