package main

import (
	"log"
	"net/http"
)

type ApiImpl struct {
}

func (a ApiImpl) CreatePage(request *http.Request, user int, search CreatePageSearch, body CreatePageBody) (*Page, error) {
	//TODO implement me
	panic("implement me")
}

func (a ApiImpl) GetPageById(request *http.Request, id int, search Tag) (*Page, error) {
	//TODO implement me
	panic("implement me")
}

func (a ApiImpl) GetPages(request *http.Request, userId int, search GetPagesSearch) (*Page, error) {
	return &Page{
		Id:    13,
		Title: "Zora",
		Alias: []string{"Peter"},
	}, nil
}

func (a ApiImpl) MyCustom(request *http.Request, writer http.ResponseWriter) {
	//TODO implement me
	panic("implement me")
}

func (a ApiImpl) PageHelp(request *http.Request, writer http.ResponseWriter) {
	_, _ = writer.Write([]byte("Pages..."))
}

func (a ApiImpl) Error(r *http.Request, w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	_, _ = w.Write([]byte(err.Error()))
}

func main() {

	server := &http.Server{
		Addr:    "0.0.0.0:4321",
		Handler: http.HandlerFunc(NewApiHandler(ApiImpl{})),
	}

	err := server.ListenAndServe()
	if err != nil {
		log.Panic(err)
	}
}
