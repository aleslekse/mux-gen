package main

import (
	"errors"
	"gopkg.in/yaml.v3"
)

type propertyType string

const (
	typeString  propertyType = "string"
	typeInteger propertyType = "integer"
	typeFloat   propertyType = "float"
	typeBoolean propertyType = "boolean"
	typeArray   propertyType = "array"
	typeModel   propertyType = "model"
	typeCustom  propertyType = "custom"
	typeEnum    propertyType = "enum"
)

type apiConfig struct {
	Package string                 `json:"package"`
	Name    string                 `json:"name"`
	Paths   map[string]*apiMethod  `yaml:"paths"`
	Models  map[string][]*apiModel `yaml:"models"`
	Types   map[string]apiType     `yaml:"types"`
	Enums   map[string][]string    `yaml:"enums"`

	modelToName  map[string]string
	enumToString map[string]string
}

type apiType struct {
	GoType   string `yaml:"go_type"`
	GoImport string `yaml:"go_import"`
	JsType   string `yaml:"js_type"`
}

type apiModelOrObject struct {
	properties []*apiModel
	model      string
}

func (m *apiModelOrObject) UnmarshalYAML(value *yaml.Node) error {
	if value.Kind == yaml.ScalarNode {
		return value.Decode(&m.model)
	} else if value.Kind == yaml.SequenceNode {
		for _, node := range value.Content {
			p := new(apiModel)
			err := node.Decode(&p)
			if err != nil {
				return err
			}
			m.properties = append(m.properties, p)
		}
		return nil
	}
	return errors.New("failed parsing model or object")
}

type apiMethod struct {
	Path     string               `yaml:"path"`
	Method   string               `yaml:"method"`
	Params   map[string]*apiModel `yaml:"params"`
	Search   *apiModelOrObject    `yaml:"search"`
	Body     *apiModelOrObject    `yaml:"body"`
	Response *apiModelOrObject    `yaml:"response"`

	searchName, bodyName, responseName string
}

type apiModel struct {
	// model, array, object, integer, float, string, boolean
	Name     string       `yaml:"name"`
	Type     propertyType `yaml:"type"`
	Model    string       `yaml:"model"`
	Children *apiModel    `yaml:"children"`
	Required bool         `yaml:"required"`
	Pointer  bool         `yaml:"pointer"`
}
