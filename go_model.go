package main

import (
	"github.com/dave/jennifer/jen"
	"github.com/iancoleman/strcase"
	"sort"
)

func genGoModels(f *jen.File, cf *apiConfig) {
	genGoEnums(f, cf)

	var names []string
	allModels := make(map[string][]*apiModel)
	for name, properties := range cf.Models {
		name = strcase.ToCamel(name)
		names = append(names, name)
		allModels[name] = properties
	}

	for name, path := range cf.Paths {
		if path.Search != nil {
			if path.Search.model == "" {
				name := strcase.ToCamel(name + "_search")
				names = append(names, name)
				allModels[name] = path.Search.properties
				path.searchName = name
			} else {
				path.searchName = strcase.ToCamel(path.Search.model)
			}

		}
		if path.Body != nil {
			if path.Body.model == "" {
				name := strcase.ToCamel(name + "_body")
				names = append(names, name)
				allModels[name] = path.Body.properties
				path.bodyName = name
			} else {
				path.bodyName = strcase.ToCamel(path.Body.model)
			}

		}
		if path.Response != nil {
			if path.Response.model == "" {
				name := strcase.ToCamel(name + "_response")
				names = append(names, name)
				allModels[name] = path.Response.properties
				path.responseName = name
			} else {
				path.responseName = strcase.ToCamel(path.Response.model)
			}
		}
	}

	sort.Strings(names)

	for _, name := range names {
		properties := allModels[name]
		f.Add(genGoModelStruct(name, properties, cf.Types))
		f.Add(genGoModelPresence(name, properties))
		f.Add(genGoQueryUnmarshaller(name, properties))
	}
}

func genGoEnums(f *jen.File, cf *apiConfig) {
	cf.enumToString = make(map[string]string)

	var names []string
	for name := range cf.Enums {
		names = append(names, name)
	}
	sort.Strings(names)

	for _, name := range names {
		goName := strcase.ToCamel(name)
		nameToStr := strcase.ToLowerCamel(name + "_to_string")
		strToName := strcase.ToLowerCamel("string_to_" + name)
		f.Type().Id(goName).String()
		f.Const().DefsFunc(func(g *jen.Group) {
			for _, value := range cf.Enums[name] {
				g.Id(strcase.ToCamel(name + "_" + value)).Op("=").Lit(value)
			}
		})
		f.Var().Id(nameToStr).Op("=").Map(jen.Id(goName)).String().Values(jen.DictFunc(func(d jen.Dict) {
			for _, value := range cf.Enums[name] {
				d[jen.Id(strcase.ToCamel(name+"_"+value))] = jen.Lit(value)
			}
		}))
		f.Var().Id(strToName).Op("=").Map(jen.String()).Id(goName).Values(jen.DictFunc(func(d jen.Dict) {
			for _, value := range cf.Enums[name] {
				d[jen.Lit(value)] = jen.Id(strcase.ToCamel(name + "_" + value))
			}
		}))
		f.Func().Params(jen.Id("e").Id(goName)).Id("Valid").Params().Bool().BlockFunc(func(g *jen.Group) {
			g.List(jen.Id("_"), jen.Id("ok")).Op(":=").Id(strToName).Index(jen.String().Parens(jen.Id("e")))
			g.Return(jen.Id("ok"))
		})
		f.Func().Params(jen.Id("e").Id(goName)).Id("String").Params().String().BlockFunc(func(g *jen.Group) {
			g.List(jen.Id("s"), jen.Id("_")).Op(":=").Id(nameToStr).Index(jen.Id("e"))
			g.Return(jen.Id("s"))
		})
		cf.enumToString[name] = strToName
	}
}

func genGoModelStruct(name string, properties []*apiModel, types map[string]apiType) *jen.Statement {
	return jen.Type().Id(name).StructFunc(func(group *jen.Group) {
		for _, property := range properties {
			group.Do(func(s *jen.Statement) {
				genGoModelProperty(s, property, types)
			})
		}
	})
}

func genGoModelType(stmt *jen.Statement, model *apiModel, types map[string]apiType) {
	if model.Pointer {
		stmt.Op("*")
	}

	if model.Type == typeModel && model.Name == "" {
		stmt.Id(strcase.ToCamel(model.Model))
	} else {
		switch model.Type {
		case typeString:
			stmt.String()
		case typeInteger:
			stmt.Int()
		case typeFloat:
			stmt.Float64()
		case typeBoolean:
			stmt.Bool()
		case typeModel:
			fallthrough
		case typeEnum:
			stmt.Id(strcase.ToCamel(model.Model))
		case typeArray:
			stmt.Index().Do(func(s *jen.Statement) {
				genGoModelType(s, model.Children, types)
			})
		case typeCustom:
			stmt.Qual(types[model.Model].GoImport, types[model.Model].GoType)
		default:
			panic("unknown type " + model.Type)
		}
	}
}

func genGoModelProperty(stmt *jen.Statement, model *apiModel, types map[string]apiType) {
	if model.Type == typeModel && model.Name == "" {
		genGoModelType(stmt, model, types)
		return
	}

	stmt.Id(strcase.ToCamel(model.Name))
	genGoModelType(stmt, model, types)
	stmt.Tag(map[string]string{
		"json": model.Name,
	})
}

func genGoModelPresence(name string, properties []*apiModel) *jen.Statement {
	return jen.Func().Params(jen.Id("m").Op("*").Id(strcase.ToCamel(name))).Id("QueryPresent").Params(
		jen.Id("prefix").String(),
		jen.Id("v").Qual("net/url", "Values"),
	).Bool().BlockFunc(func(g *jen.Group) {
		for _, property := range properties {
			addGoPropertyPresence(g, property, "")
		}
		g.Add(jen.Return(jen.Lit(false)))
	})
}

func addGoPropertyPresence(g *jen.Group, model *apiModel, prefix string) {
	switch model.Type {
	case typeArray:
	case typeModel:
		p := jen.Id("prefix")
		if model.Name != "" {
			p.Op("+").Lit(joinPath(prefix, model.Name+"."))
		}
		c := jen.Parens(jen.Op("*").Id(strcase.ToCamel(model.Model))).Dot("QueryPresent").Params(jen.Nil(), p, jen.Id("v"))
		s := jen.If(c).Block(jen.Return(jen.Lit(true)))
		g.Add(s)
	default:
		s := jen.If(jen.Id("v").Dot("Has").Call(jen.Id("prefix").Op("+").Lit(joinPath(prefix, model.Name)))).Block(jen.Return(jen.Lit(true)))
		g.Add(s)
	}
}

func genGoQueryUnmarshaller(name string, properties []*apiModel) *jen.Statement {
	return jen.Func().Params(jen.Id("m").Op("*").Id(strcase.ToCamel(name))).Id("QueryUnmarshall").Params(
		jen.Id("prefix").String(),
		jen.Id("v").Qual("net/url", "Values"),
	).Params(jen.Id("err").Error()).BlockFunc(func(g *jen.Group) {
		for _, property := range properties {
			switch property.Type {
			case typeModel:
				genGoUrlUnmarshallModel(g, property)

			case typeArray:
				if isTypeScalar(property.Type) {
					g.Add(jen.Id("m").Dot(strcase.ToCamel(property.Name)).Op("=").Nil())
					g.Add(jen.For(jen.Id("i").Op(":=").Range().Id("v").Index(jen.Id("prefix").Op("+").Lit(property.Name))).BlockFunc(func(g *jen.Group) {
						g.List(jen.Id("av"), jen.Id("ae")).Op(":=").Do(func(s *jen.Statement) {
							genGoUrlUnmarshallScalar(s, property.Children, jen.Id("v").Index(jen.Id("prefix").Op("+").Lit(property.Name)).Index(jen.Id("i")))
						})
						g.If(jen.Id("ae").Op("!=").Nil()).Block(jen.Return())
						g.Id("m").Dot(strcase.ToCamel(property.Name)).Op("=").Id("append").Call(jen.Id("m").Dot(strcase.ToCamel(property.Name)), jen.Id("av"))
					}))
				}

			case typeCustom:
				gen := func(g *jen.Group) {
					val := jen.Id("v").Dot("Get").Call(jen.Id("prefix").Op("+").Lit(property.Name))
					g.Add(jen.Id("err").Op("=").Id("m").Dot(strcase.ToCamel(property.Name)).Dot("UnmarshalText").Call(jen.Index().Byte().Parens(val)))
					g.Add(jen.If(jen.Id("err").Op("!=").Nil()).Block(jen.Return()))
				}

				if !property.Required {
					g.If(jen.Id("v").Dot("Has").Call(jen.Id("prefix").Op("+").Lit(property.Name))).BlockFunc(func(g *jen.Group) {
						gen(g)
					})
				} else {
					gen(g)
				}

			case typeEnum:
				varName := strcase.ToLowerCamel(property.Name + "_str")
				g.List(jen.Id(varName), jen.Id("err")).Op(":=").Do(func(s *jen.Statement) {
					val := jen.Id("v").Dot("Get").Call(jen.Id("prefix").Op("+").Lit(property.Name))
					genGoUrlUnmarshallScalar(s, property, val)
				})
				g.If(jen.Id("err").Op("!=").Nil()).Block(jen.Return(jen.Id("err")))
				if property.Pointer {
					g.If(jen.Id(varName).Op("!=").Nil()).BlockFunc(func(g *jen.Group) {
						g.Id("m").Dot(strcase.ToCamel(property.Name)).Op("=").Parens(jen.Op("*").Id(strcase.ToCamel(property.Model))).Parens(jen.Id(varName))
						g.Add(jen.If(jen.Op("!").Id("m").Dot(strcase.ToCamel(property.Name)).Dot("Valid").Params()).BlockFunc(func(g *jen.Group) {
							g.Return(jen.Qual("errors", "New").Params(jen.Lit("invalid " + property.Name + " value")))
						}))
					})
				} else {
					g.Id("m").Dot(strcase.ToCamel(property.Name)).Op("=").Id(strcase.ToCamel(property.Model)).Parens(jen.Id(varName))
					g.Add(jen.If(jen.Id("m").Dot(strcase.ToCamel(property.Name)).Op("!=").Lit("").Op("&&").Op("!").Id("m").Dot(strcase.ToCamel(property.Name)).Dot("Valid").Params()).BlockFunc(func(g *jen.Group) {
						g.Return(jen.Qual("errors", "New").Params(jen.Lit("invalid " + property.Name + " value")))
					}))
				}

			default:
				g.Add(jen.List(jen.Id("m").Dot(strcase.ToCamel(property.Name)), jen.Id("err"))).Op("=").Do(func(s *jen.Statement) {
					val := jen.Id("v").Dot("Get").Call(jen.Id("prefix").Op("+").Lit(property.Name))
					genGoUrlUnmarshallScalar(s, property, val)
				})
				g.Add(jen.If(jen.Id("err").Op("!=").Nil()).Block(jen.Return()))
			}
		}

		g.Add(jen.Return())
	})
}

func genGoUrlUnmarshallModel(g *jen.Group, model *apiModel) {
	var propertyName string
	if model.Name != "" {
		propertyName = strcase.ToCamel(model.Name)
	} else {
		propertyName = strcase.ToCamel(model.Model)
	}

	gen := func(g *jen.Group) {
		if model.Pointer {
			g.Add(jen.Id("m").Dot(propertyName).Op("=").New(jen.Id(strcase.ToCamel(model.Model))))
		}

		g.Add(jen.Id("err").Op("=").Id("m").Dot(strcase.ToCamel(propertyName)).Dot("QueryUnmarshall").Call(jen.Id("prefix").Do(func(s *jen.Statement) {
			if model.Name != "" {
				s.Op("+").Lit(model.Name + ".")
			}
		}), jen.Id("v")))

		g.Add(jen.If(jen.Id("err").Op("!=").Nil()).Block(jen.Return()))
	}

	if model.Pointer && !model.Required {
		g.Add(jen.If(jen.Id("m").Dot(propertyName).Dot("QueryPresent").Call(jen.Id("prefix").Do(func(s *jen.Statement) {
			if model.Name != "" {
				s.Op("+").Lit(model.Name + ".")
			}
		}), jen.Id("v"))).BlockFunc(func(g *jen.Group) {
			gen(g)
		}))
	} else {
		gen(g)
	}
}

func genGoUrlUnmarshallScalar(s *jen.Statement, model *apiModel, val *jen.Statement) {
	switch model.Type {
	case typeString:
		fallthrough
	case typeEnum:
		if model.Pointer {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallStringPointer").Call(val, jen.Lit(model.Required))
		} else {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallStringValue").Call(val, jen.Lit(model.Required))
		}
	case typeInteger:
		if model.Pointer {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallIntegerPointer").Call(val, jen.Lit(model.Required))
		} else {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallIntegerValue").Call(val, jen.Lit(model.Required))
		}
	case typeFloat:
		if model.Pointer {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallFloatPointer").Call(val, jen.Lit(model.Required))
		} else {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallFloatValue").Call(val, jen.Lit(model.Required))
		}
	case typeBoolean:
		if model.Pointer {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallBooleanPointer").Call(val, jen.Lit(model.Required))
		} else {
			s.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "UnmarshallBooleanValue").Call(val, jen.Lit(model.Required))
		}
	default:
		panic("unsupported type " + model.Type)
	}
}

func joinPath(a, b string) string {
	if a != "" && b != "" {
		return a + "." + b
	} else if a != "" {
		return a
	}
	return b
}
