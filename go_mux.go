package main

import (
	"fmt"
	"github.com/dave/jennifer/jen"
	"github.com/iancoleman/strcase"
	"gitlab.com/aleslekse/mux-gen/mux_gen"
	"sort"
)

func genGoMux(f *jen.File, cf *apiConfig) {
	allRoutes := prepareRoutes(cf)

	anyParams := false
	for _, method := range cf.Paths {
		if len(method.Params) > 0 {
			anyParams = true
		}
	}

	f.Func().Id(strcase.ToCamel("new_"+cf.Name+"_handler")).Params(jen.Id("routes").Id(strcase.ToCamel(cf.Name))).Func().Params(jen.Qual("net/http", "ResponseWriter"), jen.Op("*").Qual("net/http", "Request")).BlockFunc(func(g *jen.Group) {
		g.Return(jen.Func().Params(jen.Id("writer").Qual("net/http", "ResponseWriter"), jen.Id("request").Op("*").Qual("net/http", "Request")).BlockFunc(func(g *jen.Group) {
			g.Var().Id("idx").Int()
			g.Var().Id("name").String()
			if anyParams {
				g.Var().Id("vars").Index().String()
			}

			g.Switch(jen.Id("request").Dot("Method")).BlockFunc(func(g *jen.Group) {
				var allMethods []string
				for method := range allRoutes {
					allMethods = append(allMethods, method)
				}
				sort.Strings(allMethods)

				for _, method := range allMethods {
					g.Case(jen.Lit(method)).BlockFunc(func(g *jen.Group) {
						genGoMuxRoutes(allRoutes[method], g)
					})
				}
			})

			g.Return()
		}))
	})
}

func genGoMuxRoutes(r *route, g *jen.Group) {
	g.List(jen.Id("idx"), jen.Id("name")).Op("=").Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "SplitPath").Call(jen.Id("idx"), jen.Id("request").Dot("URL").Dot("Path"))
	g.If(jen.Id("idx").Op("==").Lit(-1)).BlockFunc(func(g *jen.Group) {
		if r.method != nil {
			genGoMuxCall(r.name, r.method, g)
		} else {
			g.Id("routes").Dot("Error").Params(jen.Id("request"), jen.Id("writer"), jen.Qual("gitlab.com/aleslekse/mux-gen/mux_gen", "ErrNotFound"))
			g.Return()
		}
	})
	if len(r.children) == 0 {
		return
	}
	g.Switch(jen.Id("name")).BlockFunc(func(g *jen.Group) {
		var segments []string
		var paramSegment string
		for segment := range r.children {
			if segment[0] != ':' {
				segments = append(segments, segment)
			} else {
				paramSegment = segment
			}
		}
		sort.Strings(segments)

		for _, segment := range segments {
			g.Case(jen.Lit(segment)).BlockFunc(func(g *jen.Group) {
				genGoMuxRoutes(r.children[segment], g)
			})
		}

		if paramSegment != "" {
			g.Default().BlockFunc(func(g *jen.Group) {
				g.Id("vars").Op("=").Id("append").Call(jen.Id("vars"), jen.Id("name"))
				genGoMuxRoutes(r.children[paramSegment], g)
			})
		}
	})
}

func genGoMuxCall(name string, m *apiMethod, g *jen.Group) {
	idx := 0
	segment := ""
	idx, segment = mux_gen.SplitPath(idx, m.Path)
	idy := 0
	varIndex := 0
	var allVars []string
	var hasErr = false
	for idx != -1 {
		if segment[0] == ':' {
			varName := fmt.Sprintf("%s%d", strcase.ToLowerCamel("var_"+segment[1:]), varIndex)
			varIndex++
			allVars = append(allVars, varName)
			g.List(jen.Id(varName), jen.Err()).Op(":=").Do(func(g *jen.Statement) {
				genGoUrlUnmarshallScalar(g, m.Params[segment[1:]], jen.Id("vars").Index(jen.Lit(idy)))
			})
			g.If(jen.Id("err").Op("!=").Nil()).BlockFunc(func(g *jen.Group) {
				g.Id("routes").Dot("Error").Params(jen.Id("request"), jen.Id("writer"), jen.Id("err"))
				g.Return()
			})
			idy++
			hasErr = true

		}
		idx, segment = mux_gen.SplitPath(idx, m.Path)
	}

	var varSearch = fmt.Sprintf("varSearch%d", varIndex)
	varIndex++
	if m.Search != nil {
		g.Var().Id(varSearch).Id(strcase.ToCamel(m.searchName))
		g.Id("err").Op(hasErrOp(&hasErr)).Id(varSearch).Dot("QueryUnmarshall").Call(jen.Lit(""), jen.Id("request").Dot("URL").Dot("Query").Call())
		g.If(jen.Id("err").Op("!=").Nil()).BlockFunc(func(g *jen.Group) {
			g.Id("routes").Dot("Error").Params(jen.Id("request"), jen.Id("writer"), jen.Id("err"))
			g.Return()
		})
	}

	var varBody = fmt.Sprintf("varBody%d", varIndex)
	varIndex++
	if m.Body != nil {
		g.Var().Id(varBody).Id(m.bodyName)
		g.Id("err").Op(hasErrOp(&hasErr)).Qual("encoding/json", "NewDecoder").Call(jen.Id("request").Dot("Body")).Dot("Decode").Call(jen.Op("&").Id(varBody))
		g.If(jen.Id("err").Op("!=").Nil()).BlockFunc(func(g *jen.Group) {
			g.Id("routes").Dot("Error").Params(jen.Id("request"), jen.Id("writer"), jen.Id("err"))
			g.Return()
		})
	}

	var responseParams = fmt.Sprintf("varResponse%d", varIndex)
	varIndex++
	if m.Response != nil {
		g.Var().Id(responseParams).Op("*").Id(m.responseName)
	}

	g.Do(func(s *jen.Statement) {
		if m.Response != nil {
			s.List(jen.Id(responseParams), jen.Id("err")).Op("=")
		}
	}).Id("routes").Dot(strcase.ToCamel(name)).CallFunc(func(g *jen.Group) {
		g.Id("request")
		for _, varName := range allVars {
			g.Id(varName)
		}
		if m.Search != nil {
			g.Id(varSearch)
		}
		if m.Body != nil {
			g.Id(varBody)
		}
		if m.Response == nil {
			g.Id("writer")
		}
	})
	if m.Response != nil {
		g.If(jen.Id("err").Op("!=").Nil()).BlockFunc(func(g *jen.Group) {
			g.Id("routes").Dot("Error").Params(jen.Id("request"), jen.Id("writer"), jen.Id("err"))
			g.Return()
		})
		g.Id("writer").Dot("Header").Call().Dot("Set").Call(jen.Lit("Content-Type"), jen.Lit("application/json"))
		g.Id("err").Op("=").Qual("encoding/json", "NewEncoder").Call(jen.Id("writer")).Dot("Encode").Call(jen.Id(responseParams))

		g.If(jen.Id("err").Op("!=").Nil()).BlockFunc(func(g *jen.Group) {
			g.Id("routes").Dot("Error").Params(jen.Id("request"), jen.Nil(), jen.Id("err"))
			g.Return()
		})
	}

	g.Return()
}

func hasErrOp(hasErr *bool) string {
	if *hasErr {
		return "="
	}
	*hasErr = true
	return ":="
}
