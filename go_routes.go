package main

import (
	"fmt"
	"github.com/dave/jennifer/jen"
	"github.com/iancoleman/strcase"
	"gitlab.com/aleslekse/mux-gen/mux_gen"
	"sort"
)

type route struct {
	children map[string]*route
	name     string
	method   *apiMethod
}

func newRoute() *route {
	return &route{
		children: make(map[string]*route),
	}
}

func prepareRoutes(cf *apiConfig) map[string]*route {
	rootRoutes := make(map[string]*route)

	for name, method := range cf.Paths {
		current, ok := rootRoutes[method.Method]
		if !ok {
			current = newRoute()
			rootRoutes[method.Method] = current
		}

		idx := 0
		for {
			var segment string
			idx, segment = mux_gen.SplitPath(idx, method.Path)
			_, ok := current.children[segment]
			if !ok {
				current.children[segment] = newRoute()
			}
			current = current.children[segment]
			if idx == len(method.Path) {
				current.name = name
				current.method = method
				break
			}
		}
	}

	return rootRoutes
}

func genRoutes(f *jen.File, cf *apiConfig) {
	f.Type().Id(strcase.ToCamel(cf.Name)).InterfaceFunc(func(g *jen.Group) {
		var names []string
		for name := range cf.Paths {
			names = append(names, name)
		}
		sort.Strings(names)

		for _, name := range names {
			method := cf.Paths[name]
			g.Comment(fmt.Sprintf("%s implements %s handler for %s", strcase.ToCamel(name), method.Method, method.Path))
			g.Id(strcase.ToCamel(name)).ParamsFunc(func(g *jen.Group) {
				g.Id("request").Op("*").Qual("net/http", "Request")
				if false {
					println(method)
				}

				idx := 0
				var segment string
				for {
					idx, segment = mux_gen.SplitPath(idx, method.Path)
					if idx == -1 {
						break
					}
					if segment[0] != ':' {
						continue
					}
					varName := segment[1:]
					varModel := method.Params[varName]

					g.Id(varName).Do(func(s *jen.Statement) {
						genGoModelType(s, varModel, cf.Types)
					})
				}

				if method.Search != nil {
					if method.Search.model != "" {
						g.Id("search").Id(strcase.ToCamel(method.Search.model))
					} else {
						g.Id("search").Id(strcase.ToCamel(name + "_Search"))
					}
				}

				if method.Body != nil {
					if method.Body.model != "" {
						g.Id("body").Id(strcase.ToCamel(method.Body.model))
					} else {
						g.Id("body").Id(strcase.ToCamel(name + "_Body"))
					}
				}

				if method.Response == nil {
					g.Id("writer").Qual("net/http", "ResponseWriter")
				}
			}).ParamsFunc(func(g *jen.Group) {
				if method.Response != nil {
					if method.Response.model != "" {
						g.Op("*").Id(strcase.ToCamel(method.Response.model))
					} else {
						g.Op("*").Id(strcase.ToCamel(name + "_Response"))
					}
					g.Id("error")
				}
			})
		}

		g.Id("Error").Params(
			jen.Id("r").Op("*").Qual("net/http", "Request"),
			jen.Id("w").Qual("net/http", "ResponseWriter"),
			jen.Id("err").Id("error"),
		)
	})
}
