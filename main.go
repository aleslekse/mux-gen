package main

import (
	"fmt"
	"github.com/dave/jennifer/jen"
	_ "github.com/iancoleman/strcase"
	"gopkg.in/yaml.v3"
	"os"
)

func main() {
	if len(os.Args) <= 1 {
		println("expected two arguments: path to config, path to output")
		os.Exit(1)
	}

	fs, err := os.Open(os.Args[1])
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
	cf := new(apiConfig)
	err = yaml.NewDecoder(fs).Decode(&cf)
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	err = validateConfig(cf)
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	f := jen.NewFile(cf.Package)

	genGoModels(f, cf)
	genRoutes(f, cf)
	genGoMux(f, cf)

	code := fmt.Sprintf("%#v", f)

	if len(os.Args) > 2 {
		err = os.WriteFile(os.Args[2], []byte(code), 0666)
		if err != nil {
			println(err.Error())
			os.Exit(1)
		}
	} else {
		println(code)
	}
}
