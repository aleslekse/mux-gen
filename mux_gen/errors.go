package mux_gen

import "errors"

var (
	ErrMissingValue        = errors.New("missing value")
	ErrInvalidBooleanValue = errors.New("invalid boolean value")
	ErrNotFound            = errors.New("not found")
	ErrBadRequest          = errors.New("bad request")
)
