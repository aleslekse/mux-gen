package mux_gen

func SplitPath(idx int, path string) (int, string) {
	if idx == -1 {
		return idx, ""
	}

	j := -1
	for i, c := range path[idx:] {
		if c == '/' {
			if j != -1 {
				k := idx + i
				for k < len(path) && path[k] == '/' {
					k++
				}
				return k, path[idx+j : idx+i]
			}
		} else if j == -1 {
			j = i
		}
	}
	if j != -1 {
		return len(path), path[idx+j:]
	}
	return -1, ""
}
