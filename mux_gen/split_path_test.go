package mux_gen

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_nextPath(t *testing.T) {

	path := "go/to"
	j, s := SplitPath(0, path)
	assert.Equal(t, "go", s)
	assert.NotEqual(t, -1, j)
	j, s = SplitPath(j, path)
	assert.Equal(t, "to", s)
	assert.Equal(t, len(path), j)
	j, s = SplitPath(j, path)
	assert.Equal(t, "", s)
	assert.Equal(t, -1, j)

	path = "//go/////to///"
	j, s = SplitPath(0, path)
	assert.Equal(t, "go", s)
	assert.NotEqual(t, -1, j)
	j, s = SplitPath(j, path)
	assert.Equal(t, "to", s)
	assert.Equal(t, len(path), j)
	j, s = SplitPath(j, path)
	assert.Equal(t, "", s)
	assert.Equal(t, -1, j)

	path = ""
	j, s = SplitPath(0, path)
	assert.Equal(t, "", s)
	assert.Equal(t, -1, j)

	path = "//////"
	j, s = SplitPath(0, path)
	assert.Equal(t, "", s)
	assert.Equal(t, -1, j)

	path = "go//////"
	j, s = SplitPath(0, path)
	assert.Equal(t, "go", s)
	assert.Equal(t, len(path), j)
	j, s = SplitPath(j, path)
	assert.Equal(t, "", s)
	assert.Equal(t, -1, j)

	path = "//////go"
	j, s = SplitPath(0, path)
	assert.Equal(t, "go", s)
	assert.Equal(t, len(path), j)
	j, s = SplitPath(j, path)
	assert.Equal(t, "", s)
	assert.Equal(t, -1, j)

	path = "foo"
	j, s = SplitPath(0, path)
	assert.Equal(t, "foo", s)
	assert.Equal(t, len(path), j)
	j, s = SplitPath(j, path)
	assert.Equal(t, "", s)
	assert.Equal(t, -1, j)

	path = "/////"
	j, s = SplitPath(0, path)
	assert.Equal(t, "", s)

}
