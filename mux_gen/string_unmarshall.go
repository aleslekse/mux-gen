package mux_gen

import (
	"strconv"
	"strings"
)

func UnmarshallIntegerValue(v string, required bool) (rv int, err error) {
	if v == "" {
		if required {
			err = ErrMissingValue
		}
		return
	}
	rv, err = strconv.Atoi(v)
	return
}

func UnmarshallIntegerPointer(v string, required bool) (*int, error) {
	if v == "" {
		if required {
			return nil, ErrMissingValue
		}
		return nil, nil
	}
	rv, err := strconv.Atoi(v)
	if err != nil {
		return nil, err
	}

	return &rv, err
}

func UnmarshallFloatValue(v string, required bool) (rv float64, err error) {
	if v == "" {
		if required {
			err = ErrMissingValue
		}
		return
	}
	rv, err = strconv.ParseFloat(v, 64)
	return
}

func UnmarshallFloatPointer(v string, required bool) (*float64, error) {
	if v == "" {
		if required {
			return nil, ErrMissingValue
		}
		return nil, nil
	}
	rv, err := strconv.ParseFloat(v, 64)
	if err != nil {
		return nil, err
	}

	return &rv, err
}

func UnmarshallStringValue(v string, required bool) (rv string, err error) {
	if v == "" && required {
		return "", ErrMissingValue
	}

	return v, nil
}

func UnmarshallStringPointer(v string, required bool) (*string, error) {
	if v == "" {
		if required {
			return nil, ErrMissingValue
		}
		return nil, nil
	}

	return &v, nil
}

func UnmarshallBooleanValue(v string, required bool) (rv bool, err error) {
	if v == "" {
		if required {
			return false, ErrMissingValue
		}
		return false, nil
	}

	if strings.EqualFold(v, "yes") || strings.EqualFold(v, "true") || strings.EqualFold(v, "on") || strings.EqualFold(v, "1") {
		return true, nil
	} else if strings.EqualFold(v, "no") || strings.EqualFold(v, "false") || strings.EqualFold(v, "off") || strings.EqualFold(v, "0") {
		return false, nil
	}

	return false, ErrInvalidBooleanValue
}

func UnmarshallBooleanPointer(v string, required bool) (*bool, error) {
	if v == "" {
		if required {
			return nil, ErrMissingValue
		}
		return nil, nil
	}

	rv, err := UnmarshallBooleanValue(v, required)
	if err != nil {
		return nil, err
	}

	return &rv, nil
}
