package main

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"
)

var httpMethods = map[string]struct{}{
	http.MethodGet:     {},
	http.MethodHead:    {},
	http.MethodPost:    {},
	http.MethodPut:     {},
	http.MethodPatch:   {},
	http.MethodDelete:  {},
	http.MethodConnect: {},
	http.MethodOptions: {},
	http.MethodTrace:   {},
}

var propertyTypes = map[propertyType]struct{}{
	typeString:  {},
	typeInteger: {},
	typeFloat:   {},
	typeBoolean: {},
	typeArray:   {},
	typeModel:   {},
	typeCustom:  {},
	typeEnum:    {},
}

var scalarTypes = map[propertyType]struct{}{
	typeString:  {},
	typeInteger: {},
	typeFloat:   {},
	typeBoolean: {},
	typeCustom:  {},
	typeEnum:    {},
}

var varNameRegex = regexp.MustCompile("(?i)^[a-z][a-z0-9]*$")

func isTypeScalar(t propertyType) bool {
	_, ok := scalarTypes[t]
	return ok
}

func validateName(name string) bool {
	return varNameRegex.Match([]byte(name))
}

func validateModel(model *apiModel, cf *apiConfig, needsName bool) error {
	if _, ok := propertyTypes[model.Type]; !ok {
		return fmt.Errorf("invalid type %s", model.Type)
	}

	if needsName {
		if model.Type != typeModel || model.Name != "" {
			validateName(model.Name)
		}
	} else if model.Name != "" {
		return errors.New("name not expected here")
	}

	if model.Type == typeModel {
		if _, ok := cf.Models[model.Model]; !ok {
			return fmt.Errorf("missing model %s", model.Model)
		}
	} else if model.Type == typeArray {
		if model.Children == nil {
			return fmt.Errorf("missing array model")
		}
		err := validateModel(model.Children, cf, false)
		if err != nil {
			return fmt.Errorf("invalid array child type for %s: %w", model.Name, err)
		}
	} else if model.Type == typeCustom {
		if _, ok := cf.Types[model.Model]; !ok {
			return fmt.Errorf("missing custom type %s", model.Model)
		}
	}

	return nil
}

func validateProperties(properties []*apiModel, cf *apiConfig, needsName bool) error {
	for _, property := range properties {
		err := validateModel(property, cf, needsName)
		if err != nil {
			return err
		}
	}
	return nil
}

func validateModelOrObject(modelOrObject *apiModelOrObject, cf *apiConfig, needsName bool) error {
	if modelOrObject.model != "" {
		if _, ok := cf.Models[modelOrObject.model]; !ok {
			return fmt.Errorf("model missing %s", modelOrObject.model)
		}
	}

	return validateProperties(modelOrObject.properties, cf, needsName)
}

func validateConfig(cf *apiConfig) error {
	if cf.Package == "" {
		cf.Package = "main"
	}

	if cf.Name == "" {
		cf.Name = "routes"
	} else {
		if !validateName(cf.Name) {
			return errors.New("invalid name")
		}
	}

	for pathName, method := range cf.Paths {
		if _, ok := httpMethods[method.Method]; !ok {
			return fmt.Errorf("Invalid method %s for path %s", method.Method, pathName)
		}
		if !validateName(pathName) {
			return fmt.Errorf("Invalid path name %s", pathName)
		}

		for paramName, model := range method.Params {
			if !validateName(paramName) {
				return fmt.Errorf("path %s param name %s invalid", pathName, paramName)
			}

			err := validateModel(model, cf, false)
			if err != nil {
				return fmt.Errorf("path %s param %s invalid: %w", pathName, paramName, err)
			}
		}

		if method.Search != nil {
			err := validateModelOrObject(method.Search, cf, true)
			if err != nil {
				return err
			}
		}

		if method.Body != nil {
			err := validateModelOrObject(method.Body, cf, true)
			if err != nil {
				return err
			}
		}

		if method.Response != nil {
			err := validateModelOrObject(method.Response, cf, true)
			if err != nil {
				return err
			}
		}
	}

	for name, properties := range cf.Models {
		if !validateName(name) {
			return fmt.Errorf("invalid name %s", name)
		}
		err := validateProperties(properties, cf, true)
		if err != nil {
			return err
		}
	}

	return nil
}
